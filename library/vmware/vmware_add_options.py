#!/usr/bin/python
# -*- coding: utf-8 -*-

DOCUMENTATION = '''
---
module: vmware_delete_all_interfaces
short_description: Adds extra config to the advanced parameters of a VMware VM.
description:
  - This module requires login to VCenter Server
  - This module requires pyVmomi python module installed
  - This module Adds extra configurations to VMs
options:
  hostname:
    description:
      - The vSphere server on which the folder is to be created
    required: true
  username:
    description:
      - The username to authenticate with, on vSphere
    required: true
  password:
    description:
      - The password to authenticate with, on vSphere
    required: true
  datacenter:
    description:
      - The name of the datacenter where the folder should be created
    required: true
  folder_name:
    description:
      - The folder name
    required: true
  name:
    description:
      The name of the VM.
    required: true
  options:
    description:
      List of the options to add in the format:
        - key: 'some_key_to_add'
          value: 'TRUE'
    required: true
'''


import sys

from ansible.module_utils.vmware import connect_to_api, find_vm_by_id, vmware_argument_spec

try:
    from pyVim.connect import SmartConnect
    from pyVim.task import WaitForTask
    from pyVmomi import vim

except ImportError:
    print "failed=true, msg=pyVmomi Python module not available"
    sys.exit(1)

try:
    from ansible.module_utils.basic import AnsibleModule
except ImportError:
    print "failed=true, msg=ansible Python module not available"
    sys.exit(1)


def create_vmware_option(key, value):
    """
    returns the pyvmomi object of the given key & value.
    :param key: String. the name of the option to add.
    :param value: String\int. the value of the option to add.
    :return: vim.option.OptionValue
    """
    opt = vim.option.OptionValue()
    opt.key = key
    opt.value = value
    return opt


def main():
    argument_spec = vmware_argument_spec()
    argument_spec.update(
        folder=dict(required=True),
        name=dict(required=True),
        options=dict(required=True, type=list)
    )
    module = AnsibleModule(argument_spec=argument_spec,
                           supports_check_mode=True)

    folder_name = module.params.get('folder')
    vm_name = module.params.get('name')
    options = module.params.get('options')
    spec = vim.vm.ConfigSpec()
    content = connect_to_api(module)
    vm = find_vm_by_id(content, vm_id=vm_name, folder=folder_name, match_first=False)

    if vm:
        for option in options:
            spec.extraConfig.append(create_vmware_option(option['key'], option['value']))

    task_state = WaitForTask(vm.ReconfigVM_Task(spec))

    if task_state == "success":
        module.exit_json(changed=True, state=task_state, vm_name=vm_name)
    else:
        module.fail_json(msg="The task failed, status = {}".format(task_state))

# <<INCLUDE_ANSIBLE_MODULE_COMMON>>
main()
