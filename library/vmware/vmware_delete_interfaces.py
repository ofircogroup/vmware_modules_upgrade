#!/usr/bin/python
# -*- coding: utf-8 -*-

DOCUMENTATION = '''
---
module: vmware_delete_interfaces
short_description: Delete the network interfaces of a vmware virtual machine
description:
  - This module requires login to VCenter Server
  - This module requires pyVmomi python module installed
  - This module removes the nics of the given vm
options:
  hostname:
    description:
      - The vSphere server on which the folder is to be created
    required: true
  username:
    description:
      - The username to authenticate with, on vSphere
    required: true
  password:
    description:
      - The password to authenticate with, on vSphere
    required: true
  folder_name:
    description:
      - The folder name
    required: true
  name:
    description:
      The name of the VM.
    required: true
  delete_all:
    description:
      Whether to delete all the interfaces or not
    required: false
  nics:
    description:
      A list of names of the NICs to remove
    required: false
'''

import sys
import time

from ansible.module_utils.vmware import (connect_to_api, find_vm_by_id, vmware_argument_spec)

try:
    from pyVim.connect import SmartConnect
    from pyVmomi import vim
except ImportError:
    print "failed=true, msg=pyVmomi Python module not available"
    sys.exit(1)
try:
    from ansible.module_utils.basic import AnsibleModule
except ImportError:
    print "failed=true, msg=ansible Python module not available"
    sys.exit(1)


def get_nics(vm, nics_names, get_all):
    """
    returns all nics of a the given vm by names in the array
    :param vm: virtual machine object of type vim.VirtualMachine
    :param nics_names: a list of the wanted nics
    :param get_all: Whether to get all the nics or not
    :return: a list of vim.vm.device.VirtualEthernetCard that the VM has
    """
    nic_list = []
    for dev in vm.config.hardware.device:
        if isinstance(dev, vim.vm.device.VirtualEthernetCard) and (get_all or dev.deviceInfo.label in nics_names):
            nic_list.append(dev)
    return nic_list


def delete_nics_by_name(vm, nics):
    """
    removes all nics for a virtual machine
    :param vm: virtual machine object of type vim.VirtualMachine
    :param nics: The nics to delete by name
    :return: if succeeded or not
    """
    changes_list = []
    for nic in nics:
        delete_operation = vim.vm.device.VirtualDeviceSpec()
        delete_operation.operation = vim.vm.device.VirtualDeviceSpec.Operation.remove
        delete_operation.device = nic
        changes_list.append(delete_operation)
    spec = vim.vm.ConfigSpec()
    spec.deviceChange = changes_list
    task = vm.ReconfigVM_Task(spec=spec)
    wait_for_task(task)
    return task.info.state == 'success'


def wait_for_task(task):
    while task.info.state not in ['error', 'success']:
        time.sleep(1)


def getvm(content=None, name=None, uuid=None, folder=None):
    vm = None
    if uuid:
        vm = find_vm_by_id(content, vm_id=uuid, vm_id_type="uuid")
    elif folder and name:
        match_first = True
        vm = find_vm_by_id(content, vm_id=name, vm_id_type="inventory_path", folder=folder,
                           match_first=match_first)

    return vm


def main():
    argument_spec = vmware_argument_spec()
    argument_spec.update(
        folder=dict(required=True),
        name=dict(required=True),
        delete_all=dict(type='bool', required=False, default=False),
        nics=dict(type='list', required=False, default=[])
    )
    module = AnsibleModule(argument_spec=argument_spec,
                           supports_check_mode=True,
                           )

    folder_name = module.params.get('folder')
    vm_name = module.params.get('name')
    nics_names = module.params.get('nics')
    delete_all = module.params.get('delete_all')

    content = connect_to_api(module)
    vm = getvm(content=content, name=vm_name, folder=folder_name)
    if vm:
        nics = get_nics(vm, nics_names, delete_all)
        if len(nics) == 0:
            module.exit_json(changed=False, vm_name=vm_name)
        else:
            if delete_nics_by_name(vm, nics):
                module.exit_json(changed=True, vm_name=vm_name)
            else:
                module.fail_json(msg="Couldn't delete nics due to unknown reason")
    else:
        module.fail_json(msg="Virtual Machine doesn't exist ")


# <<INCLUDE_ANSIBLE_MODULE_COMMON>>
main()
