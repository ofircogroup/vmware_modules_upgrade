#!/usr/bin/python
# -*- coding: utf-8 -*-
#

from __future__ import absolute_import, division, print_function

__metaclass__ = type

DOCUMENTATION = '''
---
module: port_mirroring
short_description: Add/Remove/Modify port mirroring.
description:
  - Add/Remove/Modify port mirroring on a Distributed vSwitch.
requirements:
  - "python >= 2.7"
  - ansible
options:
  hostname:
    required: true
    description:
      - The hostname or IP address of the vSphere vCenter.
  username:
    required: true
    description:
      - The username of the vSphere vCenter.
  password:
    required: true
    description:
      - The password of the vSphere vCenter.
  cluster:
    required: true
    description:
      - The cluster name for the port mirroring.
  dvs:
    required: True
    description:
      - The name of the distributed vSwitch the port mirroring should be modified on.
  state:
    required: True
    description:
      - Determines if the port_mirror should be present or not.
    type: bool
    choices:
      - 'present'
      - 'absent'
  name:
    required: True 
    description:
      - The name for the port mirroring.
  description:
    required: False
    description:
      - The description for the port mirroring.
  source_port_group:
    required: True
    description:
      - The source port group name for which transmitted packets are mirrored.
  traffic_direction:
    required: True
    description:
      - The traffic direction for the mirrored packets.
    choices:
      - 'ingress'
      - 'egress'
      - 'ingress_egress'
  destination_port_group:
    required: True
    description:
      - The port mirroring destinations port group name.
  encapsulation_vlan_id:
    required: False
    type: int
    description:
      - The vlan ID used to encapsulate the mirrored trafic.
  mirrored_packet_length:
    required: False
    type: int
    description:
      - The max mirrored packet frame length
  strip_original_vlan:
    required: False
    default: False
    type: bool
    description:
      - Whether to strip the original VLAN tag.
  override:
    required: False
    default: False
    type: bool
    description:
      - Whether to override the OWL if exists.
'''

try:
    from pyVmomi import vim, vmodl
except ImportError as e:
    pass

from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.vmware import (PyVmomi, find_dvs_by_name,
                                         vmware_argument_spec, wait_for_task, find_dvspg_by_name)

class PortMirroring(PyVmomi):

    DEFAULT_MIRRORED_PACKET_LENGTH = 9000

    def __init__(self, module):
        super(PortMirroring, self).__init__(module)
        self._warn = module.warn
        self._state = self.module.params['state']
        self._dvs = find_dvs_by_name(self.content, self.module.params['dvs'])
        self._name = self.module.params['name']
        self._description = self.module.params['description']
        self._source_port_group = self.module.params['source_port_group']
        self._traffic_direction = self.module.params['traffic_direction']
        self._destination_port_group = self.module.params['destination_port_group']
        self._encapsulation_vlan_id = self.module.params['encapsulation_vlan_id']
        self._strip_original_vlan = self.module.params['strip_original_vlan']
        self._override = self.module.params['override']
        self._mirrored_packet_length = self.module.params['mirrored_packet_length']

    def _get_vspan_session(self, dvs, name):
        for vspan_session in dvs.config.vspanSession:
            if vspan_session.name == name:
                return vspan_session
        return None

    def create(self):
        """
        Create a port mirroring
        """
        if self._dvs is None:
            return {'changed': False, 'failed': True, 'msg': 'DVS {} canno\'t be found'.format(self.module.params['dvs'])}
        mirror = self._get_vspan_session(self._dvs, self._name)
        if mirror is not None and not self._override:
            self._warn('A port mirroring by that name is already exists')
            return {'changed': False, 'failed': False}

        source_pg = find_dvspg_by_name(self._dvs, self._source_port_group)
        if source_pg is None:
            return {'changed': False, 'failed': True, 'msg': 'Source port group name canno\'t be found'}
        destination_pg = find_dvspg_by_name(self._dvs, self._destination_port_group)
        if destination_pg is None:
            return {'changed': False, 'failed': True, 'msg': 'Destination port group name canno\'t be found'}

        source_ports = vim.dvs.VmwareDistributedVirtualSwitch.VspanPorts(portKey=source_pg.portKeys)
        destination_ports = vim.dvs.VmwareDistributedVirtualSwitch.VspanPorts(portKey=destination_pg.portKeys)
        session = vim.dvs.VmwareDistributedVirtualSwitch.VspanSession(
            name=self._name, description=self._description,
            enabled=self._state == 'present', destinationPort=destination_ports, sessionType='dvPortMirror',
            stripOriginalVlan=self._strip_original_vlan, normalTrafficAllowed=False,
        )

        if self._traffic_direction == 'ingress_egress':
            session.sourcePortReceived = source_ports
            session.sourcePortTransmitted = source_ports
        elif self._traffic_direction == 'ingress':
            session.sourcePortReceived = source_ports
        else:
            session.sourcePortTransmitted = source_ports

        if self._encapsulation_vlan_id:
            session.encapsulationVlanId = self._encapsulation_vlan_id
        session.mirroredPacketLength = int(self._mirrored_packet_length) if self._mirrored_packet_length else self.DEFAULT_MIRRORED_PACKET_LENGTH
        if mirror is None:
            session_config = vim.dvs.VmwareDistributedVirtualSwitch.VspanConfigSpec(vspanSession=session, operation='add')
        else:
            session.key = mirror.key
            session_config = vim.dvs.VmwareDistributedVirtualSwitch.VspanConfigSpec(vspanSession=session, operation='edit')

        config = vim.dvs.VmwareDistributedVirtualSwitch.ConfigSpec(vspanConfigSpec=[session_config],
                                                                   configVersion=self._dvs.config.configVersion)
        wait_for_task(self._dvs.ReconfigureDvs_Task(config))
        return {'changed': True, 'failed': False}


def main():
    argument_spec = vmware_argument_spec()
    argument_spec.update(
        dict(
            dvs=dict(required=True, type='str'),
            state=dict(required=True, choices=['present', 'absent'], type='str'),
            name=dict(required=True, type='str'),
            description=dict(required=False, type='str'),
            source_port_group=dict(required=True, type='str'),
            traffic_direction=dict(required=True, choices=['ingress', 'egress', 'ingress_egress'], type='str'),
            destination_port_group=dict(required=True, type='str'),
            encapsulation_vlan_id=dict(required=False, type='int'),
            mirrored_packet_length=dict(required=False, type='int'),
            strip_original_vlan=dict(required=False, type='bool', default=False),
            override=dict(required=False, type='bool', default=False)
        )
    )

    module = AnsibleModule(argument_spec=argument_spec,
                           supports_check_mode=True)

    port_mirroring = PortMirroring(module)
    module.exit_json(**port_mirroring.create())


if __name__ == '__main__':
    main()
